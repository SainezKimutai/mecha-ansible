python = python3

.DEFAULT_GOAL = help

BLACK=\033[30m
RED=\033[31m
GREEN=\033[32m
YELLOW=\033[33m
BLUE=\033[34m
PURPLE=\033[35m
CYAN=\033[36m
WHITE=\033[37m
RESET=\033[0m

help:
	@echo "=================== Help ======================="
	@echo ""
	@echo "$(PURPLE)>$(RESET) $(GREEN) make colors  $(RESET) $(CYAN):$(RESET) $(BLUE) Check existing colors"
	@echo ""
	@echo "$(PURPLE)>$(RESET) $(GREEN) make connect $(RESET) $(CYAN):$(RESET) $(BLUE) To test reachable server"
	@echo ""
	@echo "$(PURPLE)>$(RESET) $(GREEN) make config-instance $(RESET) $(CYAN):$(RESET) $(BLUE) To  configure a new instance"
	@echo ""
	@echo "$(PURPLE)>$(RESET) $(GREEN) make config-user-service $(RESET) $(CYAN):$(RESET) $(BLUE) To configure user service"
	@echo ""
	@echo "$(PURPLE)>$(RESET) $(GREEN) make config-project-service $(RESET) $(CYAN):$(RESET) $(BLUE) To configure project service"
	@echo ""
	@echo "$(PURPLE)>$(RESET) $(GREEN) make config-redis $(RESET) $(CYAN):$(RESET) $(BLUE) To configure redis"
	@echo ""
	@echo "$(PURPLE)>$(RESET) $(GREEN) make config-celery $(RESET) $(CYAN):$(RESET) $(BLUE) To configure celery for project service"
	@echo ""
	@echo "$(PURPLE)>$(RESET) $(GREEN) make config-client $(RESET) $(CYAN):$(RESET) $(BLUE) To configure client"
	@echo ""
	@echo "$(PURPLE)>$(RESET) $(GREEN) make fix-ssh ip={host_ip} $(RESET) $(CYAN):$(RESET) $(BLUE) To fix 'REMOTE HOST IDENTIFICATION HAS CHANGED!' warning"
	@echo ""
	@echo "$(PURPLE)>$(RESET) $(GREEN) make encrypt $(RESET) $(CYAN):$(RESET) $(BLUE) To encrypt vault.yml"
	@echo ""
	@echo "$(PURPLE)>$(RESET) $(GREEN) make decrypt $(RESET) $(CYAN):$(RESET) $(BLUE) To decrypt vault.yml"
	@echo ""
	@echo "$(PURPLE)>$(RESET) $(GREEN) make config-db  $(RESET) $(CYAN):$(RESET) $(BLUE) To configure db"
.PHONY: help

colors:
	@echo "=================== Colors ======================="
	@echo ""
	@echo "$(RED)>   Halleluya $(RESET)"
	@echo ""
	@echo "$(GREEN)>   Halleluya $(RESET)"
	@echo ""
	@echo "$(YELLOW)>   Halleluya $(RESET)"
	@echo ""
	@echo "$(BLUE)>   Halleluya $(RESET)"
	@echo ""
	@echo "$(PURPLE)>   Halleluya $(RESET)"
	@echo ""
	@echo "$(CYAN)>   Halleluya $(RESET)"
	@echo ""
	@echo "$(WHITE)>   Halleluya $(RESET)"
	@echo ""
.PHONY: color

connect:
	ansible all -m ping -u sainez -i ./inventory/
.PHONY: connect

config-instance:
	ansible-playbook -u sainez -i ./inventory ./config_instance.yml
.PHONY: config

config-user-service:
	ansible-playbook -u sainez -i ./inventory ./config_user_service.yml --vault-password-file=vault-pass.txt
.PHONY: config-server

config-project-service:
	ansible-playbook -u sainez -i ./inventory ./config_project_service.yml --vault-password-file=vault-pass.txt
.PHONY: config-project-service

config-redis:
	ansible-playbook -u sainez -i ./inventory ./config_redis.yml --vault-password-file=vault-pass.txt
.PHONY: config-redis

config-celery:
	ansible-playbook -u sainez -i ./inventory ./config_celery.yml --vault-password-file=vault-pass.txt
.PHONY: config-celery

config-client:
	ansible-playbook -u sainez -i ./inventory ./config_client.yml --vault-password-file=vault-pass.txt
.PHONY: config-client

config-db:
	ansible-playbook -u sainez -i ./inventory ./config_db.yml --vault-password-file=vault-pass.txt
.PHONY: config-db

deploy:
	ansible-playbook -u sainez -i ./inventory ./mecha_deploy.yml  --vault-password-file=vault-pass.txt
.PHONY: deploy

deploy-client:
	ansible-playbook -u sainez -i ./inventory ./mecha_deploy.yml  --vault-password-file=vault-pass.txt --tags "client"
.PHONY: deploy-client

deploy-user-service:
	ansible-playbook -u sainez -i ./inventory ./mecha_deploy.yml  --vault-password-file=vault-pass.txt --tags "user_service"
.PHONY: deploy-user-service

deploy-project-service:
	ansible-playbook -u sainez -i ./inventory ./mecha_deploy.yml  --vault-password-file=vault-pass.txt --tags "project_service"
.PHONY: deploy-project-service

fix-ssh:
	ssh-keygen -R $(ip)
.PHONY: fix-shh

decrypt:
	ansible-vault decrypt vault.yml --vault-password-file=vault-pass.txt
.PHONY: config-db

encrypt:
	ansible-vault encrypt vault.yml --vault-password-file=vault-pass.txt
.PHONY: config-db